# Solarus Flatpak

> :warning: This build is not production-ready.
Sadly, it has been adandoned in favor of [solarus-snap](https://gitlab.com/solarus-games/solarus-snap) for some time.
We are always looking for more people to help out, so if you're interested in maintaining this feel free to just do it.

This a [Flatpak](https://flatpak.org/) build for Solarus.
Currently, it only builds the engine and launcher, although building the Quest Editor is also a goal.

## Building

1. If you haven't already, [install Flatpak](https://flatpak.org/setup/) and ensure you have the Flathub repository enabled
2. Install `flatpak-builder`:
```
sudo [apt|dnf|zypper|…] install flatpak-builder
```
3. Install the required KDE runtime and SDK dependencies:
```
flatpak-builder --user --install-deps-from=flathub --install-deps-only build org.solarus_games.solarus-launcher.json
```
4. Build it:
```
flatpak-builder --ccache --force-clean --repo repo build org.solarus_games.solarus-launcher.json
```
5. Run it:
```
flatpak-builder --run build org.solarus_games.solarus-launcher.json solarus-launcher
```

This launches the quest launcher, although the `solarus-run` command is also available by modifying the last command.
